## Project \#one - Bitcoin Core Dockerfile

### Build and run the Dockerfile
`./build-and-run.sh`

## Project \#two - K8s deployment manifests

### Prerequisites
Must be authenticated to an active k8s cluster (minikube, docker for desktop, etc.)

### Apply the deployment manifest
`kubectl apply -f deployment.yaml`

## Project \#three - CI pipeline
See the .gitlab-ci.yml file in the root of the project

## Project \#four - IP frequency bash script
Execute the project via `./four/ip_frequency.sh ./four/log.txt`

## Project \#five - IP frequency golang program
If Go tools are installed (from the "five" directory): 
* Run the program:`go run main.go`
* Run test suite:`go test -v`

If Go tools are not installed, and running on MacOS: `./rank`

## Project \#six - Terraform IAM scripts
Simply behold the wonder of said scripts...or if you really want to apply them then
`terraform init`
and
`terraform apply`
should do the trick. You will need to pass in the target AWS account number.

