package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"sort"
	"strings"
)

type Entry struct {
	Ip       string
	Requests int
}

func main() {

	f, err := os.Open("log.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	list := rankIps(f)

	listJson, err := json.MarshalIndent(list, "", " ")
	if err != nil {
		fmt.Printf("Error constructing json list: %s", err)
	}

	fmt.Printf("%s", listJson)
}

// Take list of requests, parse line by line for IPs and corresponding requests, sort in descending order.
func rankIps(f *os.File) []Entry {
	addresses := make(map[string]int)
	var ipList []Entry

	s := bufio.NewScanner(f)

	// Ref: https://stackoverflow.com/questions/8757389/reading-a-file-line-by-line-in-go I referred to this example implementation of scanner.scan
	// The below iterates through each token returned by s.scan (which is a line of text given that the default split func is ScanLines)
	for s.Scan() {
		request := strings.Split(s.Text(), " ")
		ip := request[1]
		addresses[ip]++
	}

	// Add addresses entries to structs, create slice of structs for easier sorting via sort.SliceStable.
	for k, v := range addresses {
		e := Entry{
			Ip:       k,
			Requests: v,
		}
		ipList = append(ipList, e)
	}

	//Ref: https://pkg.go.dev/sort#SliceStable
	sort.SliceStable(ipList, func(i, j int) bool { return ipList[i].Requests > ipList[j].Requests })

	// Final sorted list of IPs
	return ipList
}
