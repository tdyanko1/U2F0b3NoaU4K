package main

import (
	"encoding/json"
	"log"
	"os"
	"reflect"
	"testing"
)

// Verify mock data against rankIps output
func TestRankIps(t *testing.T) {

	sampleLog, err := os.Open("test-fixtures/sample-log.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer sampleLog.Close()

	mockList := []Entry{
		{"127.82.130.93", 18},
		{"139.138.227.252", 10},
		{"198.192.27.189", 6},
		{"192.168.21.34", 5},
		{"104.194.250.156", 3},
		{"171.87.2.59", 3},
		{"112.193.22.165", 2},
		{"199.25.214.114", 2},
		{"187.236.230.110", 1},
	}

	testList := rankIps(sampleLog)

	mockListJson, _ := json.MarshalIndent(mockList, "", " ")
	testListJson, _ := json.MarshalIndent(testList, "", " ")

	t.Run("Rank Ips", func(t *testing.T) {
		if !reflect.DeepEqual(testList, mockList) {
			t.Errorf("Expected %s, got %s", mockListJson, testListJson)
		}
	})

}
