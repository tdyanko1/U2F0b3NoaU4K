#!/usr/bin/env bash

# ip_frequency.sh
# A script to determine the frequency of IPs in a request log.

# Note - this script takes advantage of associative arrays which are a more recent bash feature ( bash v4 or greater)
# If running on a mac, a simple "brew install bash" should do the trick if you are behind.

set -e  -o pipefail

usage(){
  echo "Usage: ip_frequency.sh [input_file]"
}

INPUT_FILE=${1}

if [ -z $INPUT_FILE ]; then
  usage
  exit 0
fi

declare -A counts

# Build array of counts
while read -r request;
  do IP=$(echo $request | awk '{print $2}');
    # If IP doesn't exist, create an entry for it, otherwise, increment the count for that entry.
    if [ -z ${counts[$IP]} ]; then
      counts["$IP"]=1
    else
      ((counts[$IP]++))
    fi
done < $INPUT_FILE

# Explode the array, sort by number of occurences.
for IP in "${!counts[@]}";
  do
    echo "$IP: ${counts[$IP]}"
done | sort -rn -k2 #Ref: https://stackoverflow.com/questions/8217049/bash-associative-array-sorting-by-value Perform a numeric sort against associative array based on the 2nd column, in reverse.


