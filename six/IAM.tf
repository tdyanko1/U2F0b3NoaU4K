#A role, with no permissions, which can be assumed by users within the same account.
resource "aws_iam_role" "dev-ci" {
  name = "dev-ci"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          "AWS": "arn:aws:iam::${var.aws_account_number}:root"
        }
      },
    ]
  })
}

#User tyanko who would like to assume the dev-ci role 
resource "aws_iam_user" "tyanko_at_protonmail_com" {
  name = "tyanko@protonmail.com"
}

#A policy, allowing users / entities to assume the above role.
resource "aws_iam_policy" "assume_dev_ci_role" {
  name = "AssumeDevCiRole"
  
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "sts:AssumeRole",
        ]
        Effect   = "Allow"
        Resource = "arn:aws:iam::${var.aws_account_number}:role/dev-ci"
      },
    ]
  })
}

#A group, with the above policy attached
resource "aws_iam_group" "dev_ci" {
  name = "DevCi"
}

resource "aws_iam_group_policy_attachment" "dev_ci" {
  group = aws_iam_group.dev_ci.name
  policy_arn = aws_iam_policy.assume_dev_ci_role.arn
}

#A user belonging to the above group
resource "aws_iam_group_membership" "dev_ci_group" {
  name = "dev_ci_users"
  group = aws_iam_group.dev_ci.name

  users = [
    aws_iam_user.tyanko_at_protonmail_com.name,
  ]
}
