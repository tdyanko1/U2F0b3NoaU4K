terraform {
  required_version = "~> 1.2.5"

  required_providers {
    aws = {
      version = "4.23.0"
    }
  }
}